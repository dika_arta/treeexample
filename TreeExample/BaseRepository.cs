﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace TreeExample
{
    public abstract class BaseRepository
    {
        protected static IDbConnection OpenConnection()
        {
            var dbConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["DS"].ToString());
            dbConnection.Open();
            return dbConnection;
        }
    }
}
