﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TreeExample
{
    public static class TreeService
    {
        public static IList<TreeModel> BuildTree(this IEnumerable<TreeModel> source)
        {
            var groups = source.GroupBy(i => i.ParentId).ToList();

            var roots = groups.FirstOrDefault(g => g.Key.HasValue == false).ToList();

            if (roots.Count > 0)
            {
                var dict = groups.Where(g => g.Key.HasValue).ToDictionary(g => g.Key.Value, g => g.ToList());
                for (int i = 0; i < roots.Count; i++)
                    AddChildren(roots[i], dict);
            }

            return roots;
        }

        private static void AddChildren(TreeModel node, IDictionary<int, List<TreeModel>> source)
        {
            if (source.ContainsKey(node.ID))
            {
                node.ChildList = source[node.ID];
                for (int i = 0; i < node.ChildList.Count; i++)
                    AddChildren(node.ChildList[i], source);
            }
            else
            {
                node.ChildList = new List<TreeModel>();
            }
        }
    }
}
