﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TreeExample
{
    public class TreeModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int? ParentId { get; set; }
        public List<TreeModel> ChildList { get; set; }
    }
}
