USE [Example]

DROP TABLE [dbo].[Tree]

CREATE TABLE [dbo].[Tree](
	[ID] [int] NOT NULL,
	[Name] [nvarchar](50) NULL,
	[ParentId] [int] NULL,
 CONSTRAINT [PK_Tree] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
INSERT [dbo].[Tree] ([ID], [Name], [ParentId]) VALUES (1, N'Level_1', NULL)
GO
INSERT [dbo].[Tree] ([ID], [Name], [ParentId]) VALUES (2, N'Level_1_1', 1)
GO
INSERT [dbo].[Tree] ([ID], [Name], [ParentId]) VALUES (3, N'Level_1_2', 1)
GO
INSERT [dbo].[Tree] ([ID], [Name], [ParentId]) VALUES (4, N'Level_1_1_1', 2)
GO
INSERT [dbo].[Tree] ([ID], [Name], [ParentId]) VALUES (5, N'Level_1_1_2', 2)
GO
INSERT [dbo].[Tree] ([ID], [Name], [ParentId]) VALUES (6, N'Level_1_2_1', 3)
GO
INSERT [dbo].[Tree] ([ID], [Name], [ParentId]) VALUES (7, N'Level_1_2_2', 3)
GO
INSERT [dbo].[Tree] ([ID], [Name], [ParentId]) VALUES (8, N'Level_1_2_2_1', 7)
GO
INSERT [dbo].[Tree] ([ID], [Name], [ParentId]) VALUES (9, N'Level_1_2_1_1', 6)
GO
USE [master]
GO
ALTER DATABASE [Example] SET  READ_WRITE 
GO
