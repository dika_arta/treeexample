﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dapper;

namespace TreeExample
{
    public interface ITreeRepository
    {
        TreeModel GetById(int Id);
        List<TreeModel> GetByParentId(int parentId);
        bool IsHaveChild(int Id);
        List<TreeModel> GetAll();
    }
    public class TreeRepository : BaseRepository, ITreeRepository
    {
        public List<TreeModel> GetAll()
        {
            using (var con = OpenConnection())
            {
                var query = "SELECT * FROM Tree";
                var res = con.Query<TreeModel>(query).ToList();
                return res;
            }
        }

        public TreeModel GetById(int Id)
        {
            using (var con = OpenConnection())
            {
                var query = "SELECT * FROM Tree WHERE ID=@Id";
                var res = con.Query<TreeModel>(query, new { Id = Id }).SingleOrDefault();
                return res;
            }
        }

        public List<TreeModel> GetByParentId(int parentId)
        {
            using (var con = OpenConnection())
            {
                var query = "SELECT * FROM Tree WHERE ParentId=@parentId";
                var res = con.Query<TreeModel>(query, new { parentId = parentId }).ToList();
                return res;
            }
        }

        public bool IsHaveChild(int Id)
        {
            using (var con = OpenConnection())
            {
                var query = "SELECT * FROM Tree WHERE ParentId=@Id";
                var res = con.Query<TreeModel>(query, new { Id=Id }).ToList();
                if(res.Any())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}
