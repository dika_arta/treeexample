﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft;
using System.Reflection;
using System.Dynamic;

namespace TreeExample
{
    class Program
    {
        static void Main(string[] args)
        {
            ITreeRepository repo = new TreeRepository();
            var all = repo.GetAll();
            var tree = all.BuildTree();
            var treeJson = Newtonsoft.Json.JsonConvert.SerializeObject(tree, Newtonsoft.Json.Formatting.Indented);
            Console.WriteLine(treeJson);

            Console.ReadLine();
        }
    }
}
